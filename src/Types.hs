module Types
  ( Feed(..)
  , Colon(..), unColon
  , BillName
  , Bill(..)
  , Category(..)
  , DistrictName
  , District(..)
  , CategoryAmount(..)
  , SpecificAllocation(..)
  , Solution(..)
  , BillSolution(..)
  , DistrictFunding(..)
  , DistrictStatus(..)
  )
  where

import Data.Aeson
import Data.Aeson.Types (Parser)
import GHC.Generics
import qualified Data.Text as T
import qualified Data.Char as C
import Foreign.Storable (Storable)
------------------------------------------------------------------------------

newtype Colon = Colon { unColon :: Int }
  deriving (Eq, Show, Ord, Num, Real, Integral, Enum, Generic, ToJSON, FromJSON, Storable)

------------------------------------------------------------------------------

--Input datatype
data Feed = Feed
  { fBills :: [Bill]
  , fDistricts :: [District]
  } deriving (Eq, Show, Generic)

instance FromJSON Feed where
  parseJSON = prefixedParseJSON

instance ToJSON Feed where
  toJSON = prefixedToJSON
------------------------------------------------------------------------------

newtype Category = Category { unCategory :: String }
    deriving (Eq, Show, Generic, ToJSON, FromJSON)

------------------------------------------------------------------------------

type BillName = T.Text

data Bill = Bill
  { bName :: BillName
  , bCategory :: Category 
  , bAmount :: Colon
  } deriving (Eq, Show, Generic)

instance FromJSON Bill where
  parseJSON = prefixedParseJSON

instance ToJSON Bill where
  toJSON = prefixedToJSON

------------------------------------------------------------------------------
  
type DistrictName = T.Text
data District = District
  { dName :: DistrictName
  , dAvailableFunds :: Colon
  , dCategoryDefaultFunding :: [CategoryAmount]
  , dBillSpecificFunding :: [SpecificAllocation]
  , dCaps :: [CategoryAmount]
  } deriving (Eq, Show, Generic)

instance FromJSON District where
  parseJSON = prefixedParseJSON

instance ToJSON District where
  toJSON = prefixedToJSON

------------------------------------------------------------------------------

data CategoryAmount = CategoryAmount
  { caCategory :: Category
  , caAmount :: Colon
  } deriving (Eq, Show, Generic)

instance FromJSON CategoryAmount where
  parseJSON = prefixedParseJSON

instance ToJSON CategoryAmount where
  toJSON = prefixedToJSON

------------------------------------------------------------------------------

data SpecificAllocation = SpecificAllocation
  { saBill :: T.Text
  , saAmount :: Colon
  } deriving (Eq, Show, Generic)

instance FromJSON SpecificAllocation where
  parseJSON = prefixedParseJSON

instance ToJSON SpecificAllocation where
  toJSON = prefixedToJSON

------------------------------------------------------------------------------

-- The type we are solving for
data Solution = Solution { sBillSolutions :: [BillSolution], sDistrictStatuses :: [DistrictStatus] }
  deriving (Eq, Show, Generic)

instance FromJSON Solution where
  parseJSON = prefixedParseJSON

instance ToJSON Solution where
  toJSON = prefixedToJSON

-- A solution for a single bill
data BillSolution = BillSolution { bsUnderage :: Colon, bsBill :: T.Text, bsDistrictFundings :: [DistrictFunding] }
  deriving (Eq, Show, Generic)

instance FromJSON BillSolution where
  parseJSON = prefixedParseJSON

instance ToJSON BillSolution where
  toJSON = prefixedToJSON

-- Funding for a bill/district pair
data DistrictFunding = DistrictFunding 
  { dfDistrict :: T.Text
  , dfFunding :: Colon
  } deriving (Eq, Show, Generic)

instance FromJSON DistrictFunding where
  parseJSON = prefixedParseJSON

instance ToJSON DistrictFunding where
  toJSON = prefixedToJSON

data DistrictStatus = DistrictStatus { dsDistrict :: T.Text, dsAmountRemaining :: Colon }
  deriving (Eq, Show, Generic)

instance FromJSON DistrictStatus where
  parseJSON = prefixedParseJSON

instance ToJSON DistrictStatus where
  toJSON = prefixedToJSON
----------------------------------------------------------------------------

-- JSON util functions
prefixedParseJSON :: (Generic a, GFromJSON Zero (Rep a)) => Value -> Parser a
prefixedParseJSON = genericParseJSON defaultOptions { fieldLabelModifier = dePrefix }

prefixedToJSON :: (Generic a, GToJSON Zero (Rep a)) => a -> Value
prefixedToJSON = genericToJSON defaultOptions { fieldLabelModifier = dePrefix }

dePrefix :: String -> String
dePrefix (x : xs) 
  | C.isLower x = dePrefix xs
  | otherwise = (C.toLower x) : xs
dePrefix x = x
