module Lib (solvedFunding)  where

import Types
import Data.Ord (comparing)
import Data.List (find, foldl', maximumBy, partition, null)
import Data.Maybe (fromMaybe, isJust)
import Control.Applicative ((<|>))
import Numeric.LinearAlgebra.Data (Matrix, fromLists, toLists, (><), tr)
import Numeric.LinearAlgebra (scale, Element)
import Control.Monad (guard)

solvedFunding :: [Bill] -> [District] -> Solution
solvedFunding bills districts = Solution solutions districtStatuses
  where solutions = billSolutions bills districts (weightings bills districts)
        districtStatuses = [DistrictStatus dName dAvailableFunds | District{..} <- foldl' deFund districts solutions]

-- Calculate funds left in districts after applying a bill
deFund :: [District] -> BillSolution -> [District]
deFund districts (BillSolution _ _ dfs) = deFund' <$> zip districts dfs
  where deFund' (district, funding) = district { dAvailableFunds = (dAvailableFunds district) - (dfFunding funding) }
  
-- An nBills x nDistricts Weightings of each district's desired spend, constructed by averaging between interdistrict and interbill ratios
weightings :: [Bill] -> [District] -> Matrix Double
weightings bills districts = normalizeMx . scale 0.5 $ (mkMx interDistrictScale) * (mkMx interBillScale)
  where mkMx f = (length bills >< length districts) [dbl (desiredFunding d b) / dbl (f d b)  | d <- districts, b <- bills]
        interDistrictScale _ b = totalDesiredBill b
        interBillScale d _ = totalDesiredDistrict d

        totalDesiredBill :: Bill -> Colon
        totalDesiredBill bill = sum $ (flip desiredFunding bill) <$> districts

        totalDesiredDistrict :: District -> Colon
        totalDesiredDistrict district = sum $ desiredFunding district <$> bills

        normalizeMx :: (Element t, Fractional t) => Matrix t -> Matrix t
        normalizeMx mx = fromLists $ normalize 1 <$> (toLists mx) 

--How much a district intends to spend on a bill, by category or overridden specifically
desiredFunding :: District -> Bill -> Colon
desiredFunding district bill = fromMaybe (Colon 0) (billSpecificAllocation <|> billDefaultFromCat)
  where billDefaultFromCat = caAmount <$> find (\cdf -> caCategory cdf == bCategory bill) (dCategoryDefaultFunding district)
        billSpecificAllocation = saAmount <$> find (\sa -> saBill sa == bName bill) (dBillSpecificFunding district)

-- The result of partitioning a solution into satisfied and unsatisfied bills
data SolutionSplit = SolutionSplit
  { ssBills :: [Bill] -- Unsatisfied bills to be processed
  , ssWeights :: Matrix Double -- Corresponding weights (length ssBills x nDistricts)
  , ssFunded :: [BillSolution] -- Satisfied billsolutions
  }


-- Computes optimal payments for our inputs and computed weightings
-- Strategy:
 -- Pick scaling factor (initial is high enough to ensure all bills can be paid - ie maximum bill cost)
  -- For scaling factor try:
   -- scale weightings out to factor to determine a payment matrix
   -- cap by category
   -- apply constraint - doesn't put a district into the negative
   -- apply terminating condition as constraint - at least one bill must be not be overfunded
   -- refund overpayments of other bills 
  -- When matching scaling factor is found, partition bills into satisfied and unsatisfied
  -- Repeat against unsatisfied bills
  -- Terminate once all bills are satisfied, or we cannot satisfy any more
billSolutions :: [Bill] -> [District] -> Matrix Double -> [BillSolution]
billSolutions bills [] _ = [ BillSolution (bAmount bill) (bName bill) [] | bill <- bills ]
billSolutions [] _ _ = []
billSolutions bills districts weights
  | null (ssFunded split) = allSolutions
  | otherwise = (ssFunded split) ++ (billSolutions (ssBills split) (deFunded (ssFunded split)) (ssWeights split))
  where split :: SolutionSplit
        split = splitSolutions bills weights allSolutions

        deFunded solutions = foldl' deFund districts solutions
        
        allSolutions :: [BillSolution]
        allSolutions = zipWith (\b p -> BillSolution (underage b (districtFundings p)) (bName b) (districtFundings p)) bills maxPayments

        districtFundings :: [Colon] -> [DistrictFunding]
        districtFundings = zipWith districtFunding districts 

        startingScale :: Double
        startingScale = fromIntegral . bAmount $ maximumBy (comparing bAmount) bills

        maxPayments :: [[Colon]]
        maxPayments = head $ do
          base <- reverse [0..startingScale]
          let potentialByBill = scale base weights
          let potentialByDistrict = tr potentialByBill
          let categoryCapped = categoryCap bills districts (toLists potentialByDistrict)
          let byDistrict = colonify categoryCapped                    
          let byBill = colonify . toLists . tr . fromLists $ categoryCapped
          guard . not $ drainsAnyDistrict districts byDistrict
          guard $ atLeastOneNotOverfunded bills byBill
          pure $ refundExcess bills byBill

districtFunding :: District -> Colon -> DistrictFunding
districtFunding district payment = DistrictFunding (dName district) payment

colonify :: RealFrac a => [[a]] -> [[Colon]]
colonify x = (Colon . round <$>) <$> x

-- Seperate sufficiently funded from underfunded
splitSolutions :: [Bill] -> Matrix Double -> [BillSolution] -> SolutionSplit
splitSolutions bills weights solutions = SolutionSplit remainingBills remainingWeights funded 
  where joined = zip3 bills (toLists weights) solutions
        (fundedTrp, underFundedTrp) = partition (\(_, _, s) -> bsUnderage s <= 0) joined 
        remainingBills = [ b | (b, _, _) <- underFundedTrp ]
        remainingWeights = fromLists [ w | (_, w, _) <- underFundedTrp ]
        funded = [ bs | (_, _, bs) <- fundedTrp ]
        
-- how far a bill is from being satisfied
underage :: Bill -> [DistrictFunding] -> Colon
underage bill fundings = (bAmount bill) - (sum $ dfFunding <$> fundings)

-- Whether or not proposed payments will cause a district's funds to dip into the negative
drainsAnyDistrict :: [District] -> [[Colon]] -> Bool
drainsAnyDistrict districts payments = isJust $ find drainsDistrict (zip districts payments)
  where drainsDistrict (district, billPayments) = sum billPayments > dAvailableFunds district

-- Update a nDistrix x nBill payments matrix by normalizing bill spends to ensure the totals dont exceed category caps
categoryCap :: [Bill] -> [District] -> [[Double]] -> [[Double]]
categoryCap bills districts payments = zipWith applyLimit districts payments
  where applyLimit :: District -> [Double] -> [Double]
        applyLimit district billPayments =  zipWith (limitPayment district billPayments) bills billPayments
        
        limitPayment :: District -> [Double] -> Bill -> Double -> Double
        limitPayment district billPayments bill payment = maybe payment (\lr -> min payment (payment / lr)) (limitRatio district billPayments (bCategory bill))

        categories = bCategory <$> bills

        limitRatio :: District -> [Double] -> Category -> Maybe Double
        limitRatio district billPayments category = ratio category billPayments <$> districtLimit district category
        ratio category billPayments limit = (sum $ matchingPayments category (zip categories billPayments)) / dbl limit

        matchingPayments :: Category -> [(Category, Double)] -> [Double] 
        matchingPayments category pairs = [ billPayment | (c, billPayment) <- pairs, c == category ]

        districtLimit :: District -> Category -> Maybe Colon
        districtLimit district category = caAmount <$> find (\ca -> caCategory ca == category) (dCaps district)

-- Is there a bill in the list that isn't exceeded required spend
atLeastOneNotOverfunded :: [Bill] -> [[Colon]] -> Bool
atLeastOneNotOverfunded bills payments = isJust $ find billNotOverfunded (zip bills payments)
  where billNotOverfunded (bill, districtPayments) = sum districtPayments <= bAmount bill

-- Scale a row such that its total == to
normalize :: Fractional a => a -> [a] -> [a]
normalize to items = [ i * (to / (sum items)) | i <- items]

-- Normalize down payments to the point where bill funding is just met
refundExcess :: [Bill] -> [[Colon]] -> [[Colon]]
refundExcess bills payments = zipWith refunded bills payments
  where refunded bill ps
          | sum normed < sum ps = normed
          | otherwise = ps
              where normed = Colon . round <$> (normalize (dbl $ bAmount bill) (dbl <$> ps))

dbl :: (Integral a) => a -> Double
dbl = fromIntegral
