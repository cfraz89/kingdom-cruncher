module Main where

import Lib
import Types
import Options.Applicative
import Data.Monoid ((<>))
import Data.Aeson (eitherDecode)
import Data.Aeson.Encode.Pretty (encodePretty)
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Lazy.Char8 as C8

newtype Options = Options { oInputFile :: String }

main :: IO ()
main = do
  Options{..} <- execParser optionsParserInfo
  inputFile <- B.readFile oInputFile
  case eitherDecode inputFile of
    Left msg -> putStrLn $ "Couldn't parse input file: " <> msg
    Right Feed{..} -> C8.putStrLn . encodePretty $ solvedFunding fBills fDistricts

optionsParserInfo :: ParserInfo Options
optionsParserInfo = info (optionsParser <**> helper) fullDesc
  where optionsParser = Options <$> strArgument (metavar "JSON_FILE" <> help "Input file to parse")
