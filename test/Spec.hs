import Test.Framework (defaultMain, testGroup)
import Test.Framework.Providers.QuickCheck2 (testProperty)

import Test.QuickCheck
import Types
import qualified Data.Text as T
import Data.Maybe (isNothing, fromJust, isJust)
import Data.List (find, (\\), foldl', nub, nubBy)
import Lib
import Control.Monad (guard)
import Data.Ord (comparing)
import Data.Function(on)

main :: IO ()
main = defaultMain tests

instance Arbitrary Feed where
  arbitrary = do
    categories <- resize 5 $ nub <$> listOf1 arbitrary 
    bills' <- resize 10 $ listOf $ do
      cat <- elements categories
      name <- arbitrary
      cost <- genPositiveColon
      pure $ Bill name cat cost
    bills <- nubBy ((==) `on` bName) <$> sublistOf bills'
    let districts'' = do
          name <- arbitrary
          availableFunds <- genPositiveColon
          categoryDefaultFunding <- flip mapM categories $ \cat -> CategoryAmount cat <$> genPositiveColon
          billSpecificFunding <- flip mapM bills $ \b -> SpecificAllocation (bName b) <$> genPositiveColon
          caps <- flip mapM categories $ \c -> CategoryAmount c <$> genPositiveColon
          sublistCaps <- sublistOf caps
          pure $ District name availableFunds categoryDefaultFunding billSpecificFunding sublistCaps
    districts' <- nubBy ((==) `on` dName) <$> resize 5 (listOf districts'')
    districts <- sublistOf districts'
    pure $ Feed bills districts

instance Arbitrary Category where
  arbitrary = Category <$> arbitrary

genPositiveColon = Colon <$> choose (0, 1000000)

instance Arbitrary T.Text where
  arbitrary = T.pack <$> arbitrary

tests = [
      testGroup "Correctly divides funds" [
        testProperty "Doesn't exceed district funding" belowDistrictFunding,
        testProperty "Funds aren't overallocated" fundsNotOverallocated,
        testProperty "Solution contains all bills" solutionContainsAllBills
        ]
      ]

doesNotExist predicate = isNothing . (find predicate)
billSolutions Feed{..} = sBillSolutions $ solvedFunding fBills fDistricts

belowDistrictFunding feed@Feed{..} = doesNotExist (aboveFunding $ billSolutions feed) fDistricts
  where aboveFunding solutions District{..} = (fundingFrom (fundingsOf dName solutions)) > dAvailableFunds
        fundingFrom districtFundings = sum $ dfFunding <$> districtFundings
        fundingsOf districtName solutions = do
          solution <- solutions
          df <- bsDistrictFundings solution
          guard $ (dfDistrict df) == districtName
          pure df

fundsNotOverallocated feed@Feed{..} = doesNotExist aboveBillCost (billSolutions feed)
  where aboveBillCost BillSolution{..} = billFunding bsDistrictFundings > bAmount (billBy bsBill)
        billFunding = sum . (dfFunding <$>) 
        billBy billName = fromJust $ find ((==) billName . bName) fBills

solutionContainsAllBills feed@Feed{..} = length ((bName <$> fBills) \\ (bsBill <$> billSolutions feed)) == 0
  
